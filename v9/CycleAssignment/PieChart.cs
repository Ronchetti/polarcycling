﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace CycleAssignment
{
    public partial class PieChart : Form
    {

        String Fname = "";
        int hr1, hr2, hr3, hr4, hr5, hr6, min, max, maxhr;
        int z1, z2, z3, z4, z5, z6 = 0;
        Form1 data;
        public PieChart(string name, int xmin, int xmax, int maxheart, string state)
        {
            InitializeComponent();
            Fname = name;
            min = xmin;
            max = xmax;
            maxhr = maxheart;

            if (state == "heart")
            {
                PieSlices();
            }
            else if (state == "power")
            {
                PowerSlices();
            }
        }

        private void PieChart_Load(object sender, EventArgs e)
        {

        }
        private void PowerSlices()
        {
            int rowcount, heartrate = 0;
            double hr, maxh;
            data = new Form1(Fname);
            maxh = Convert.ToDouble(maxhr);

            //http://www.bikeradar.com/gear/article/heart-rate-monitor-training-for-cyclists-28838/
            //z1 60-65%
            hr = maxh / 100;
            hr = hr * 0;
            hr = Math.Round(hr, 0);
            hr1 = Convert.ToInt32(hr);

            //z2 65-75
            hr = maxh / 100;
            hr = hr * 56;
            hr = Math.Round(hr, 0);
            hr2 = Convert.ToInt32(hr);

            //z3 75, 82
            hr = maxh / 100;
            hr = hr * 76;
            hr = Math.Round(hr, 0);
            hr3 = Convert.ToInt32(hr);

            //z4 82, 89
            hr = maxh / 100;
            hr = hr * 91;
            hr = Math.Round(hr, 0);
            hr4 = Convert.ToInt32(hr);

            //z5 89 - 94
            hr = maxh / 100;
            hr = hr * 106;
            hr = Math.Round(hr, 0);
            hr5 = Convert.ToInt32(hr);

            //z6 94-100
            hr = maxh / 100;
            hr = hr * 121;
            hr = Math.Round(hr, 0);
            hr6 = Convert.ToInt32(hr);

            rowcount = data.dgvData.RowCount;
            if (rowcount < max)
            {
                max = rowcount;
            }

            for (int i = min; i < max; ++i)
            {
                heartrate = Convert.ToInt32(data.dgvData.Rows[i].Cells[0].Value);

                if (heartrate >= hr6)
                {
                    z6++;
                }
                else if (heartrate >= hr5)
                {
                    z5++;
                }
                else if (heartrate >= hr4)
                {
                    z4++;
                }
                else if (heartrate >= hr3)
                {
                    z3++;
                }
                else if (heartrate >= hr2)
                {
                    z2++;
                }
                else if (heartrate >= hr1)
                {
                    z1++;
                }

            }


            CreatePie(PieChart1);
        }
        private void PieSlices()
        {
            int rowcount, heartrate = 0;
            double hr, maxh;
            data = new Form1(Fname);
            maxh = Convert.ToDouble(maxhr);

            //http://www.bikeradar.com/gear/article/heart-rate-monitor-training-for-cyclists-28838/
            //z1 60-65%
            hr = maxh / 100;
            hr = hr * 60;
            hr = Math.Round(hr, 0);
            hr1 = Convert.ToInt32(hr);

            //z2 65-75
            hr = maxh / 100;
            hr = hr * 65;
            hr = Math.Round(hr, 0);
            hr2 = Convert.ToInt32(hr);

            //z3 75, 82
            hr = maxh / 100;
            hr = hr * 75;
            hr = Math.Round(hr, 0);
            hr3 = Convert.ToInt32(hr);

            //z4 82, 89
            hr = maxh / 100;
            hr = hr * 82;
            hr = Math.Round(hr, 0);
            hr4 = Convert.ToInt32(hr);

            //z5 89 - 94
            hr = maxh / 100;
            hr = hr * 89;
            hr = Math.Round(hr, 0);
            hr5 = Convert.ToInt32(hr);

            //z6 94-100
            hr = maxh / 100;
            hr = hr * 94;
            hr = Math.Round(hr, 0);
            hr6 = Convert.ToInt32(hr);

            rowcount = data.dgvData.RowCount;
            if (rowcount < max)
            {
                max = rowcount;
            }

            for (int i = min; i < max; ++i)
            {
                heartrate = Convert.ToInt32(data.dgvData.Rows[i].Cells[0].Value);

                if (heartrate >= hr6)
                {
                    z6++;
                }
                else if (heartrate >= hr5)
                {
                    z5++;
                }
                else if (heartrate >= hr4)
                {
                    z4++;
                }
                else if (heartrate >= hr3)
                {
                    z3++;
                }
                else if (heartrate >= hr2)
                {
                    z2++;
                }
                else if (heartrate >= hr1)
                {
                    z1++;
                }

            }


            CreatePie(PieChart1);
        }

        public void CreatePie(ZedGraphControl zgc)
        {
            GraphPane myPane = zgc.GraphPane;

            // Set the pane title 
            myPane.Title.Text = "Heart rate zones";         

            // Fill the pane and axis background with solid color 
            myPane.Fill = new Fill(Color.White);
            myPane.Chart.Fill = new Fill(Color.White);
            myPane.Legend.Position = LegendPos.Right;

            // Create some pie slices 
            PieItem segment1 = myPane.AddPieSlice(z6, Color.Red, .1, "Zone 6");
            PieItem segment2 = myPane.AddPieSlice(z5, Color.DarkOrange, .1, "Zone 5");
            PieItem segment3 = myPane.AddPieSlice(z4, Color.Goldenrod, .1, "Zone 4");
            PieItem segment4 = myPane.AddPieSlice(z3, Color.LimeGreen, .1, "Zone 3");
            PieItem segment5 = myPane.AddPieSlice(z2, Color.Aquamarine, .1, "Zone 2");
            PieItem segment6 = myPane.AddPieSlice(z1, Color.Teal, .1, "Zone 1");
  
            segment1.LabelType = PieLabelType.Name_Percent;
            segment2.LabelType = PieLabelType.Name_Percent;
            segment3.LabelType = PieLabelType.Name_Percent;
            segment4.LabelType = PieLabelType.Name_Percent;
            segment5.LabelType = PieLabelType.Name_Percent;
            segment6.LabelType = PieLabelType.Name_Percent;
            segment1.LabelDetail.FontSpec.FontColor = Color.Red;

            // Sum up the values																					 
            CurveList curves = myPane.CurveList;
            double total = 0;
            for (int x = 0; x < curves.Count; x++)
                total += ((PieItem)curves[x]).Value;

            // Add a text item to highlight total sales 
            /*TextObj text = new TextObj("Total 2004 Sales - " + "$" + total.ToString() + "M", 0.85F, 0.80F, CoordType.PaneFraction);
            text.Location.AlignH = AlignH.Center;
            text.Location.AlignV = AlignV.Bottom;
            text.FontSpec.Border.IsVisible = false;
            text.FontSpec.Fill = new Fill(Color.White, Color.PowderBlue, 45F);
            text.FontSpec.StringAlignment = StringAlignment.Center;
            myPane.GraphObjList.Add(text);

            // Create a drop shadow for the total value text item
            TextObj text2 = new TextObj(text);
            text2.FontSpec.Fill = new Fill(Color.Black);
            text2.Location.X += 0.008f;
            text2.Location.Y += 0.01f;
            myPane.GraphObjList.Add(text2);*/

            zgc.AxisChange(); 
        }
    }
}
