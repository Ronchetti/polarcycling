﻿using System;
using System.Windows;
using CycleAssignment;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CycleTest
{
    
    [TestClass]
    public class CycleData
    {


        [TestMethod]
        public void DateTest1()
        {
           //CycleAssignment.Form1 TestForm = new Form1();
          string expected = "05/02/2013";
          string input = "20130205";         
           
           DateMaker TestDate = new DateMaker();
           string actual = TestDate.Dateoutput(input);

           Assert.AreEqual(expected, actual,"Not calculating date correctly");

        }

        private string dategrabber(string input)
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        public void DateTest2()
        {
            //CycleAssignment.Form1 TestForm = new Form1();
            string expected = "Invalid Length";
            string input = "1234567";

            DateMaker TestDate = new DateMaker();
            string actual = TestDate.Dateoutput(input);

            Assert.AreEqual(expected, actual, "Not calculating date correctly");


        }
        [TestMethod]
        public void SmodeReader1()
        {
            string expected = " miles";
            string input = "111111110";

            smodetester Units = new smodetester();
            string actual = Units.smodetest(input);

            Assert.AreEqual(expected, actual, "Not reading smode correctly");

        }
        [TestMethod]
        public void SmodeReader2()
        {
            string expected = " km";
            string input = "111111100";

            smodetester Units = new smodetester();
            string actual = Units.smodetest(input);

            Assert.AreEqual(expected, actual, "Not reading smode correctly");

        }

        [TestMethod]
        public void Units1()
        {
            double expected = 0.621371192;
            bool input = true;

            units multiplier = new units();
            double actual = multiplier.unitsetter(input);


            Assert.AreEqual(expected, actual, "Not reading smode correctly");

        }

        [TestMethod]
        public void Units2()
        {
            double expected = 1.609344;
            bool input = false;

            units multiplier = new units();
            double actual = multiplier.unitsetter(input);


            Assert.AreEqual(expected, actual, "Not reading smode correctly");

        }
    }
}
