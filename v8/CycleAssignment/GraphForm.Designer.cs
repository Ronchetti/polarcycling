﻿namespace CycleAssignment
{
    partial class GraphForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtMaxspeed = new System.Windows.Forms.TextBox();
            this.txtAvgHR = new System.Windows.Forms.TextBox();
            this.txtAvgspeed = new System.Windows.Forms.TextBox();
            this.txtDistance = new System.Windows.Forms.TextBox();
            this.txtMaxAlt = new System.Windows.Forms.TextBox();
            this.txtAvgalt = new System.Windows.Forms.TextBox();
            this.txtMin = new System.Windows.Forms.TextBox();
            this.txtMax = new System.Windows.Forms.TextBox();
            this.txtAvgpwr = new System.Windows.Forms.TextBox();
            this.txtMaxpwr = new System.Windows.Forms.TextBox();
            this.lblMinX = new System.Windows.Forms.Label();
            this.lblMaxX = new System.Windows.Forms.Label();
            this.lblDistance = new System.Windows.Forms.Label();
            this.lblMaxalt = new System.Windows.Forms.Label();
            this.lblMaxspeed = new System.Windows.Forms.Label();
            this.lblAvgHr = new System.Windows.Forms.Label();
            this.lblavgspeed = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblAveragepwr = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMinHr = new System.Windows.Forms.TextBox();
            this.txtMaxHr = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.cbHR = new System.Windows.Forms.CheckBox();
            this.cbspeed = new System.Windows.Forms.CheckBox();
            this.cbcadence = new System.Windows.Forms.CheckBox();
            this.cbpower = new System.Windows.Forms.CheckBox();
            this.cbaltitude = new System.Windows.Forms.CheckBox();
            this.allCalculationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Location = new System.Drawing.Point(12, 30);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.Padding = new System.Windows.Forms.Padding(0, 20, 0, 0);
            this.zedGraphControl1.ScrollGrace = 0D;
            this.zedGraphControl1.ScrollMaxX = 0D;
            this.zedGraphControl1.ScrollMaxY = 0D;
            this.zedGraphControl1.ScrollMaxY2 = 0D;
            this.zedGraphControl1.ScrollMinX = 0D;
            this.zedGraphControl1.ScrollMinY = 0D;
            this.zedGraphControl1.ScrollMinY2 = 0D;
            this.zedGraphControl1.Size = new System.Drawing.Size(1091, 363);
            this.zedGraphControl1.TabIndex = 0;
            this.zedGraphControl1.Resize += new System.EventHandler(this.zedGraphControl1_Resize);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.dataToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1115, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewDataToolStripMenuItem,
            this.openToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // viewDataToolStripMenuItem
            // 
            this.viewDataToolStripMenuItem.Name = "viewDataToolStripMenuItem";
            this.viewDataToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.viewDataToolStripMenuItem.Text = "View data";
            this.viewDataToolStripMenuItem.Click += new System.EventHandler(this.viewDataToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // dataToolStripMenuItem
            // 
            this.dataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.allCalculationsToolStripMenuItem});
            this.dataToolStripMenuItem.Name = "dataToolStripMenuItem";
            this.dataToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.dataToolStripMenuItem.Text = "Data";
            // 
            // txtMaxspeed
            // 
            this.txtMaxspeed.Location = new System.Drawing.Point(791, 561);
            this.txtMaxspeed.Name = "txtMaxspeed";
            this.txtMaxspeed.Size = new System.Drawing.Size(100, 20);
            this.txtMaxspeed.TabIndex = 2;
            // 
            // txtAvgHR
            // 
            this.txtAvgHR.Location = new System.Drawing.Point(1003, 469);
            this.txtAvgHR.Name = "txtAvgHR";
            this.txtAvgHR.Size = new System.Drawing.Size(100, 20);
            this.txtAvgHR.TabIndex = 2;
            // 
            // txtAvgspeed
            // 
            this.txtAvgspeed.Location = new System.Drawing.Point(791, 514);
            this.txtAvgspeed.Name = "txtAvgspeed";
            this.txtAvgspeed.Size = new System.Drawing.Size(100, 20);
            this.txtAvgspeed.TabIndex = 2;
            // 
            // txtDistance
            // 
            this.txtDistance.Location = new System.Drawing.Point(1003, 422);
            this.txtDistance.Name = "txtDistance";
            this.txtDistance.Size = new System.Drawing.Size(100, 20);
            this.txtDistance.TabIndex = 2;
            // 
            // txtMaxAlt
            // 
            this.txtMaxAlt.Location = new System.Drawing.Point(1003, 561);
            this.txtMaxAlt.Name = "txtMaxAlt";
            this.txtMaxAlt.Size = new System.Drawing.Size(100, 20);
            this.txtMaxAlt.TabIndex = 2;
            // 
            // txtAvgalt
            // 
            this.txtAvgalt.Location = new System.Drawing.Point(1003, 514);
            this.txtAvgalt.Name = "txtAvgalt";
            this.txtAvgalt.Size = new System.Drawing.Size(100, 20);
            this.txtAvgalt.TabIndex = 2;
            // 
            // txtMin
            // 
            this.txtMin.Location = new System.Drawing.Point(791, 422);
            this.txtMin.Name = "txtMin";
            this.txtMin.Size = new System.Drawing.Size(100, 20);
            this.txtMin.TabIndex = 2;
            // 
            // txtMax
            // 
            this.txtMax.Location = new System.Drawing.Point(791, 469);
            this.txtMax.Name = "txtMax";
            this.txtMax.Size = new System.Drawing.Size(100, 20);
            this.txtMax.TabIndex = 2;
            // 
            // txtAvgpwr
            // 
            this.txtAvgpwr.Location = new System.Drawing.Point(897, 514);
            this.txtAvgpwr.Name = "txtAvgpwr";
            this.txtAvgpwr.Size = new System.Drawing.Size(100, 20);
            this.txtAvgpwr.TabIndex = 2;
            // 
            // txtMaxpwr
            // 
            this.txtMaxpwr.Location = new System.Drawing.Point(897, 561);
            this.txtMaxpwr.Name = "txtMaxpwr";
            this.txtMaxpwr.Size = new System.Drawing.Size(100, 20);
            this.txtMaxpwr.TabIndex = 2;
            // 
            // lblMinX
            // 
            this.lblMinX.AutoSize = true;
            this.lblMinX.Location = new System.Drawing.Point(791, 406);
            this.lblMinX.Name = "lblMinX";
            this.lblMinX.Size = new System.Drawing.Size(34, 13);
            this.lblMinX.TabIndex = 3;
            this.lblMinX.Text = "Min X";
            // 
            // lblMaxX
            // 
            this.lblMaxX.AutoSize = true;
            this.lblMaxX.Location = new System.Drawing.Point(791, 453);
            this.lblMaxX.Name = "lblMaxX";
            this.lblMaxX.Size = new System.Drawing.Size(37, 13);
            this.lblMaxX.TabIndex = 3;
            this.lblMaxX.Text = "Max X";
            // 
            // lblDistance
            // 
            this.lblDistance.AutoSize = true;
            this.lblDistance.Location = new System.Drawing.Point(1000, 406);
            this.lblDistance.Name = "lblDistance";
            this.lblDistance.Size = new System.Drawing.Size(49, 13);
            this.lblDistance.TabIndex = 3;
            this.lblDistance.Text = "Distance";
            // 
            // lblMaxalt
            // 
            this.lblMaxalt.AutoSize = true;
            this.lblMaxalt.Location = new System.Drawing.Point(1000, 545);
            this.lblMaxalt.Name = "lblMaxalt";
            this.lblMaxalt.Size = new System.Drawing.Size(64, 13);
            this.lblMaxalt.TabIndex = 3;
            this.lblMaxalt.Text = "Max altitude";
            // 
            // lblMaxspeed
            // 
            this.lblMaxspeed.AutoSize = true;
            this.lblMaxspeed.Location = new System.Drawing.Point(791, 545);
            this.lblMaxspeed.Name = "lblMaxspeed";
            this.lblMaxspeed.Size = new System.Drawing.Size(59, 13);
            this.lblMaxspeed.TabIndex = 3;
            this.lblMaxspeed.Text = "Max speed";
            // 
            // lblAvgHr
            // 
            this.lblAvgHr.AutoSize = true;
            this.lblAvgHr.Location = new System.Drawing.Point(1003, 453);
            this.lblAvgHr.Name = "lblAvgHr";
            this.lblAvgHr.Size = new System.Drawing.Size(95, 13);
            this.lblAvgHr.TabIndex = 3;
            this.lblAvgHr.Text = "Average heart rate";
            // 
            // lblavgspeed
            // 
            this.lblavgspeed.AutoSize = true;
            this.lblavgspeed.Location = new System.Drawing.Point(788, 498);
            this.lblavgspeed.Name = "lblavgspeed";
            this.lblavgspeed.Size = new System.Drawing.Size(79, 13);
            this.lblavgspeed.TabIndex = 3;
            this.lblavgspeed.Text = "Average speed";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1000, 498);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Average altitude";
            // 
            // lblAveragepwr
            // 
            this.lblAveragepwr.AutoSize = true;
            this.lblAveragepwr.Location = new System.Drawing.Point(894, 498);
            this.lblAveragepwr.Name = "lblAveragepwr";
            this.lblAveragepwr.Size = new System.Drawing.Size(79, 13);
            this.lblAveragepwr.TabIndex = 3;
            this.lblAveragepwr.Text = "Average power";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(894, 545);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Max Power";
            // 
            // txtMinHr
            // 
            this.txtMinHr.Location = new System.Drawing.Point(897, 422);
            this.txtMinHr.Name = "txtMinHr";
            this.txtMinHr.Size = new System.Drawing.Size(100, 20);
            this.txtMinHr.TabIndex = 2;
            // 
            // txtMaxHr
            // 
            this.txtMaxHr.Location = new System.Drawing.Point(897, 469);
            this.txtMaxHr.Name = "txtMaxHr";
            this.txtMaxHr.Size = new System.Drawing.Size(100, 20);
            this.txtMaxHr.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(897, 406);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Min heart rate";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(897, 453);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Max heart rate";
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(539, 422);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 4;
            // 
            // cbHR
            // 
            this.cbHR.AutoSize = true;
            this.cbHR.Checked = true;
            this.cbHR.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbHR.Location = new System.Drawing.Point(12, 424);
            this.cbHR.Name = "cbHR";
            this.cbHR.Size = new System.Drawing.Size(73, 17);
            this.cbHR.TabIndex = 5;
            this.cbHR.Text = "Heart rate";
            this.cbHR.UseVisualStyleBackColor = true;
            this.cbHR.CheckedChanged += new System.EventHandler(this.cbHR_CheckedChanged);
            // 
            // cbspeed
            // 
            this.cbspeed.AutoSize = true;
            this.cbspeed.Checked = true;
            this.cbspeed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbspeed.Location = new System.Drawing.Point(12, 447);
            this.cbspeed.Name = "cbspeed";
            this.cbspeed.Size = new System.Drawing.Size(57, 17);
            this.cbspeed.TabIndex = 5;
            this.cbspeed.Text = "Speed";
            this.cbspeed.UseVisualStyleBackColor = true;
            this.cbspeed.CheckedChanged += new System.EventHandler(this.cbspeed_CheckedChanged);
            // 
            // cbcadence
            // 
            this.cbcadence.AutoSize = true;
            this.cbcadence.Checked = true;
            this.cbcadence.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbcadence.Location = new System.Drawing.Point(12, 470);
            this.cbcadence.Name = "cbcadence";
            this.cbcadence.Size = new System.Drawing.Size(69, 17);
            this.cbcadence.TabIndex = 5;
            this.cbcadence.Text = "Cadence";
            this.cbcadence.UseVisualStyleBackColor = true;
            this.cbcadence.CheckedChanged += new System.EventHandler(this.cbcadence_CheckedChanged);
            // 
            // cbpower
            // 
            this.cbpower.AutoSize = true;
            this.cbpower.Checked = true;
            this.cbpower.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbpower.Location = new System.Drawing.Point(12, 492);
            this.cbpower.Name = "cbpower";
            this.cbpower.Size = new System.Drawing.Size(56, 17);
            this.cbpower.TabIndex = 5;
            this.cbpower.Text = "Power";
            this.cbpower.UseVisualStyleBackColor = true;
            this.cbpower.CheckedChanged += new System.EventHandler(this.cbpower_CheckedChanged);
            // 
            // cbaltitude
            // 
            this.cbaltitude.AutoSize = true;
            this.cbaltitude.Checked = true;
            this.cbaltitude.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbaltitude.Location = new System.Drawing.Point(12, 514);
            this.cbaltitude.Name = "cbaltitude";
            this.cbaltitude.Size = new System.Drawing.Size(61, 17);
            this.cbaltitude.TabIndex = 5;
            this.cbaltitude.Text = "Altitude";
            this.cbaltitude.UseVisualStyleBackColor = true;
            this.cbaltitude.CheckedChanged += new System.EventHandler(this.cbaltitude_CheckedChanged);
            // 
            // allCalculationsToolStripMenuItem
            // 
            this.allCalculationsToolStripMenuItem.Name = "allCalculationsToolStripMenuItem";
            this.allCalculationsToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.allCalculationsToolStripMenuItem.Text = "All Calculations";
            this.allCalculationsToolStripMenuItem.Click += new System.EventHandler(this.allCalculationsToolStripMenuItem_Click);
            // 
            // GraphForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 598);
            this.Controls.Add(this.cbaltitude);
            this.Controls.Add(this.cbpower);
            this.Controls.Add(this.cbcadence);
            this.Controls.Add(this.cbspeed);
            this.Controls.Add(this.cbHR);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblAveragepwr);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblavgspeed);
            this.Controls.Add(this.lblAvgHr);
            this.Controls.Add(this.lblMaxspeed);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblMaxalt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblDistance);
            this.Controls.Add(this.lblMaxX);
            this.Controls.Add(this.lblMinX);
            this.Controls.Add(this.txtAvgpwr);
            this.Controls.Add(this.txtMaxpwr);
            this.Controls.Add(this.txtAvgalt);
            this.Controls.Add(this.txtAvgspeed);
            this.Controls.Add(this.txtMax);
            this.Controls.Add(this.txtMaxHr);
            this.Controls.Add(this.txtMaxAlt);
            this.Controls.Add(this.txtAvgHR);
            this.Controls.Add(this.txtMin);
            this.Controls.Add(this.txtMinHr);
            this.Controls.Add(this.txtDistance);
            this.Controls.Add(this.txtMaxspeed);
            this.Controls.Add(this.zedGraphControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "GraphForm";
            this.Text = "GraphForm";
            this.Load += new System.EventHandler(this.GraphForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataToolStripMenuItem;
        private System.Windows.Forms.TextBox txtMaxspeed;
        private System.Windows.Forms.TextBox txtAvgHR;
        private System.Windows.Forms.TextBox txtAvgspeed;
        private System.Windows.Forms.TextBox txtDistance;
        private System.Windows.Forms.TextBox txtMaxAlt;
        private System.Windows.Forms.TextBox txtAvgalt;
        private System.Windows.Forms.TextBox txtMin;
        private System.Windows.Forms.TextBox txtMax;
        private System.Windows.Forms.TextBox txtAvgpwr;
        private System.Windows.Forms.TextBox txtMaxpwr;
        private System.Windows.Forms.Label lblMinX;
        private System.Windows.Forms.Label lblMaxX;
        private System.Windows.Forms.Label lblDistance;
        private System.Windows.Forms.Label lblMaxalt;
        private System.Windows.Forms.Label lblMaxspeed;
        private System.Windows.Forms.Label lblAvgHr;
        private System.Windows.Forms.Label lblavgspeed;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblAveragepwr;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtMinHr;
        private System.Windows.Forms.TextBox txtMaxHr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.CheckBox cbHR;
        private System.Windows.Forms.CheckBox cbspeed;
        private System.Windows.Forms.CheckBox cbcadence;
        private System.Windows.Forms.CheckBox cbpower;
        private System.Windows.Forms.CheckBox cbaltitude;
        private System.Windows.Forms.ToolStripMenuItem allCalculationsToolStripMenuItem;
    }
}