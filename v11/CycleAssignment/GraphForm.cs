﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace CycleAssignment
{
    public partial class GraphForm : Form
    {
        string Fname = @"I:\ASEb\test.txt";
        bool HR = true;
        bool SP = true;
        bool CA = true;
        bool ALT = true;
        bool PO = true;
        bool NP = true;
        double xmin, inc, xmax;
        int rowcount;
        Form1 form1;

        public GraphForm()
        {
            InitializeComponent();
            openfile();
        }

        private void viewDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 dataform = new Form1(Fname);
            form1.Show();
        }

        private void GraphForm_Load(object sender, EventArgs e)
        {
            // Setup the graph
            CreateGraph(zedGraphControl1);
            // Size the control to fill the form with a margin
            //SetSize();
            txtMin.Text = zedGraphControl1.GraphPane.XAxis.Scale.Min.ToString();
            txtMax.Text = zedGraphControl1.GraphPane.XAxis.Scale.Max.ToString();
            Calculationsloader();
        }

        public void openfile()
        {
            int open = 0;
            //DialogResult File = openFileDialog1.ShowDialog();
            OpenFileDialog theDialog = new OpenFileDialog();
            theDialog.Title = "Open Text File";
            //theDialog.Filter = "TXT files|*.txt";
            theDialog.InitialDirectory = @"I:\ASEb\";
            if (theDialog.ShowDialog(this) == DialogResult.OK)
            {
                //Assigns the file name to a global string then reads the file
                Fname = theDialog.FileName.ToString();
                open = 1;
                form1 = new Form1(Fname);
            }
            if (open > 0)
            {
                CreateGraph(zedGraphControl1);
            }
        }

        private void zedGraphControl1_Resize(object sender, EventArgs e)
        {
            //SetSize();
        }

        // SetSize() is separate from Resize() so we can 
        // call it independently from the Form1_Load() method
        // This leaves a 10 px margin around the outside of the control
        // Customize this to fit your needs
        /*private void SetSize()
        {
            zedGraphControl1.Location = new Point(12, 30);
            // Leave a small margin around the outside of the control
            zedGraphControl1.Size = new Size(ClientRectangle.Width - 20,
                                    ClientRectangle.Height - 40);
        }*/

        private void Calculationsloader()
        {
            xmin = Convert.ToDouble(txtMin.Text);
            xmax = Convert.ToDouble(txtMax.Text);

            //Rounding up and down
            xmin = Math.Floor(xmin);
            xmax = Math.Ceiling(xmax);

            Calculations(xmin, xmax);

        }

        public double NormalisedPower(int x, int backx)
        {
            int Amount = x - backx;
            int z = 0;
            //int backx = x - 30;
            double check = 0;
            double average = 0;
            double NormalisedPower = 0;
            long  powerholder = 0;
            int count = 0;
            double[] Values = new double[Amount];
            //1) starting at the 30 s mark, calculate a rolling 30 s average (of the preceeding time points, obviously). 
            
            for (int i = backx; i < x; ++i)
            {                
                Values[z] = Convert.ToDouble(form1.dgvData.Rows[i].Cells[4].Value);
                z++;
            }
            z = 0;
            //2) raise all the values obtained in step #1 to the 4th power. 
            //power*power*power*power
            for (int j = backx; j < x; ++j)
            {
              
                powerholder = Convert.ToInt32(Values[z]);
                powerholder = powerholder * powerholder * powerholder * powerholder;
                Values[z] = powerholder;
                z++;
            }
            powerholder = 0; z = 0;
            //3) take the average of all of the values obtained in step #2. 
            for (int k = backx; k < x; ++k)
            {
                
                powerholder = powerholder + Convert.ToInt64(Values[z]);
                count = count + 1;
                z++;
            }
            if (count >=  30)
            {
                average = powerholder / count;
            }
            //4) take the 4th root of the value obtained in step #3. 
            int l = 0;
            while (l < 2)
            {
                average = (System.Math.Sqrt(average));
                l++;
            }

            return average;
        }

        private void chart_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {
           
            txtMin.Text = sender.GraphPane.XAxis.Scale.Min.ToString();
            xmin = Convert.ToDouble(txtMin.Text);

            txtMax.Text = sender.GraphPane.XAxis.Scale.Max.ToString();
            xmax = Convert.ToDouble(txtMax.Text);

            //Rounding up and down
            xmin = Math.Floor(xmin);
            xmax = Math.Ceiling(xmax);

            txtMin.Text = xmin.ToString();
            txtMax.Text = xmax.ToString();
            Calculations(xmin, xmax);
        }

        private void Calculations(double xmin, double xmax)
        {
            int min = Convert.ToInt32(xmin);

            ClearTxtBoxes();

            rowcount = form1.dgvData.RowCount;
            if (rowcount < xmax)
            {
                xmax = rowcount;
            }

            txtDistance.Text = DistanceCalc(min, xmax).ToString();

            if (SP)
            {
                txtAvgspeed.Text = average(min, xmax, 1).ToString();
                txtMaxspeed.Text = Max(min, xmax, 1).ToString();
            }


            if (ALT)
            {
                txtAvgalt.Text = average(min, xmax, 3).ToString();
                txtMaxAlt.Text = Max(min, xmax, 3).ToString();
            }

            if (HR)
            {
                txtAvgHR.Text = average(min, xmax, 0).ToString();
                txtMaxHr.Text = Max(min, xmax, 0).ToString();
                txtMinHr.Text = Minheart(min, xmax).ToString();
            }

            if (PO)
            {
                txtAvgpwr.Text = average(min, xmax, 4).ToString();
                txtMaxpwr.Text = Max(min, xmax, 4).ToString();
            }

            if (NP)
            {
                txtNP.Text = Convert.ToString(Math.Round(NormalisedPower(Convert.ToInt32(xmax), min)));
                IntensityFactor();
            }
        }

        private void IntensityFactor()
        {
            double IF;
            double NP;
            double FTP = 0;
           // NP/FTP
            NP = Convert.ToDouble(txtNP.Text);
            //FTP = Convert.ToInt32(txtpwrzone.Text);
            if (txtpwrzone.TextLength > 3)
            {
                FTP = 320;
            }
            IF = NP / FTP;

            txtIF.Text = Convert.ToString(IF);
            TSS(IF,NP, FTP);
        }

        private void TSS(double IF, double NP, double FTP)
        {
            double duration = form1.dgvData.Rows.Count;
            duration = duration * inc;
            double TSS = 0;

            TSS = ((duration * NP * IF) / (FTP * 3600)) * 100;

            TSS = Math.Round(TSS);
            txtTSS.Text = Convert.ToString(TSS);

            IF = Math.Round(IF, 2);
            txtIF.Text = Convert.ToString(IF);

        }
        private void ClearTxtBoxes()
        {
            txtAvgalt.Text = "";
            txtMaxAlt.Text = "";

            txtAvgHR.Text = "";
            txtMaxHr.Text = "";
            txtMinHr.Text = "";

            txtMaxspeed.Text = "";
            txtAvgspeed.Text = "";

            txtAvgpwr.Text = "";
            txtMaxpwr.Text = "";
        }
        public double DistanceCalc(int min, double xmax)
        {
            //Total speed / 3600
            double tdistance = 0;

       

            for (int i = min; i < xmax; ++i)
            {
                tdistance += Convert.ToDouble(form1.dgvData.Rows[i].Cells[1].Value);
            }
            tdistance = tdistance / 3600;
            tdistance = Math.Round(tdistance, 2);
            return tdistance;
        }

        public double average(int min, double xmax, int columnindex)
        {
            double sum = 0;
            int j = 0;
            for (int i = min; i < xmax; ++i)
            {
                sum += Convert.ToDouble(form1.dgvData.Rows[i].Cells[columnindex].Value);
                j++;
            }
            sum = sum / j;
            sum = Math.Round(sum, 2);
            return sum;
        }


        public double Max(int min, double xmax, int columnindex)
        {
           // int columnindex = 1;
           // Double maxspeed = form1.dgvData.Rows.Cast<DataGridViewRow>().Max(r => Convert.ToDouble(r.Cells[columnindex].Value));
           // return maxspeed;
            double maxval = 0;
               for (int i = min; i < xmax; ++i)
               {
                   if (Convert.ToDouble(form1.dgvData.Rows[i].Cells[columnindex].Value) > maxval)
                   {
                      maxval =   Convert.ToDouble(form1.dgvData.Rows[i].Cells[columnindex].Value);
                   }
   
               }
            return maxval;
        }



        public double Minheart(int min, double xmax)
        {
            int columnindex = 0, minHR;
            double heart, miniHR =  Max(min, xmax, 0);

            minHR = Convert.ToInt16(miniHR);

            for (int i = min; i < xmax; ++i)
            {
                heart = Convert.ToDouble(form1.dgvData.Rows[i].Cells[columnindex].Value);
                if (Convert.ToInt32(heart) < minHR && Convert.ToInt32(heart) != 0)
                {
                    minHR = Convert.ToInt32(heart);
                }
            }
            return minHR;


            /*foreach (DataGridViewRow Rows in form1.dgvData.Rows)
            {
                heart = (string)Rows.Cells[0].Value;
                if (Convert.ToInt32(heart) < minheart && Convert.ToInt32(heart) != 0)
                {
                    minheart = Convert.ToInt32(heart);
                    return minheart;
                }
                return minheart;
            }
            return minheart;*/
        }


        // Build the Chart
        private void CreateGraph(ZedGraphControl zgc)
        {
            

            // get a reference to the GraphPane
            GraphPane myPane = zgc.GraphPane;
            myPane.CurveList.Clear();
            myPane.GraphObjList.Clear();

            // Set the Titles
            myPane.Title.Text = "Polar cycling";
            myPane.XAxis.Title.Text = "Time";
            //myPane.YAxis.Title.Text = "My Y Axis";

            /*myPane.XAxis.Type = AxisType.Date;
            DateTime X = new DateTime();
            X = Form1.DataContainer.StartTime;*/

            //HR speed cadence alt power power balance time

            // Make up some data arrays based on the Sine function
            double x, y1, y2, y3, y4, y5, y6;
            double totalnp = 0;
            y6 = 0;
            inc = Convert.ToDouble(form1.txtinterval.Text);
            x = 0;
            PointPairList listHR = new PointPairList();
            PointPairList listSp = new PointPairList();
            PointPairList listCad = new PointPairList();
            PointPairList listAlt = new PointPairList();
            PointPairList listPow = new PointPairList();
            PointPairList listNP = new PointPairList();

            //Add power and distance
            for (int i = 0; i < form1.dgvData.Rows.Count; ++i)
            {
                if (x >= 30)
                {
                   y6 = NormalisedPower(Convert.ToInt32(x), Convert.ToInt32(x-30));
                   totalnp = totalnp + y6;
                }

                //Heart rate
                y1 = Convert.ToDouble(form1.dgvData.Rows[i].Cells[0].Value);
                //Speed
                y2 = Convert.ToDouble(form1.dgvData.Rows[i].Cells[1].Value);
                //Cadence
                y3 = Convert.ToDouble(form1.dgvData.Rows[i].Cells[2].Value);
                //Altitude
                y4 = Convert.ToDouble(form1.dgvData.Rows[i].Cells[3].Value);
                //Power
                y5 = Convert.ToDouble(form1.dgvData.Rows[i].Cells[4].Value);

                listHR.Add(x, y1);
                listSp.Add(x, y2);
                listCad.Add(x, y3);
                listAlt.Add(x, y4);
                listPow.Add(x, y5);
                listNP.Add(x, y6);
                x = x + inc;

            }

            //Generate curve
            if (HR == true)
            {
                LineItem heart = myPane.AddCurve("Heart rate",
                      listHR, Color.Red, SymbolType.None);
            }

            if (SP == true)
            {
                LineItem speed = myPane.AddCurve("Speed",
                      listSp, Color.Blue, SymbolType.None);
            }

            if (CA == true)
            {
                LineItem cadence = myPane.AddCurve("Cadence",
                   listCad, Color.Indigo, SymbolType.None);
            }

            if (ALT == true)
            {
                LineItem altitude = myPane.AddCurve("Altitude",
                   listAlt, Color.Green, SymbolType.None);
            }

            if (PO == true)
            {
                LineItem power = myPane.AddCurve("Power",
                   listPow, Color.DarkMagenta, SymbolType.None);
            }

            if (NP == true)
            {
                LineItem NPower = myPane.AddCurve("Normalised Power",
                          listNP, Color.Orange, SymbolType.None);
            }

            // Tell ZedGraph to refigure the
            // axes since the data have changed
            totalnp = totalnp / x;
            //txtNP.Text = Convert.ToString(totalnp);
            myPane.XAxis.Scale.Max = x + 20;
            zgc.AxisChange();
            zgc.Refresh();
            zgc.ZoomEvent += chart_ZoomEvent;

        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openfile();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void HRTool_Click(object sender, EventArgs e)
        {
       
        }

        private void SPTool_Click(object sender, EventArgs e)
        {
   
        }

        private void CATool_Click(object sender, EventArgs e)
        {
        
        }

        private void ALTTool_Click(object sender, EventArgs e)
        {
 
        }

        private void POTool_Click(object sender, EventArgs e)
        {

        }

        private void cbHR_CheckedChanged(object sender, EventArgs e)
        {

            if (cbHR.Checked == true)
            {
                HR = true;
            }
            else
                HR = false;
            CreateGraph(zedGraphControl1);
            Calculations(xmin, xmax);
        }

        private void cbspeed_CheckedChanged(object sender, EventArgs e)
        {
            if (cbspeed.Checked == true)
            {
                SP = true;
            }
            else
                SP = false;
            CreateGraph(zedGraphControl1);
            Calculations(xmin, xmax);

        }

        private void cbcadence_CheckedChanged(object sender, EventArgs e)
        {
            if (cbcadence.Checked == true)
            {
                CA = true;
            }
            else
                CA = false;
            CreateGraph(zedGraphControl1);
            Calculations(xmin, xmax);

        }

        private void cbpower_CheckedChanged(object sender, EventArgs e)
        {
            if (cbpower.Checked == true)
            {
                PO = true;
            }
            else
                PO = false;
            CreateGraph(zedGraphControl1);
            Calculations(xmin, xmax);

        }

        private void cbaltitude_CheckedChanged(object sender, EventArgs e)
        {
            if (cbaltitude.Checked == true)
            {
                ALT = true;
            }
            else
                ALT = false;
            CreateGraph(zedGraphControl1);
            Calculations(xmin, xmax);

        }

        private void txtpwrzone_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnheartzone_Click(object sender, EventArgs e)
        {
            bool check = true;
            if (txtHrzone.Text == "Max heart rate")
            {
                MessageBox.Show("Please enter a max heart rate");
                check = false;
            }
            if (txtHrzone.TextLength > 2 && check == true)
            {
                int maxhr;
                maxhr = Convert.ToInt32(txtHrzone.Text);
                int min, max;
                min = Convert.ToInt32(xmin);
                max = Convert.ToInt32(xmax);
                PieChart heart = new PieChart(Fname, min, max, maxhr, "heart");
                heart.Show();
            }
            else if (check == true)
            {
                MessageBox.Show("Please enter a max heart rate");
            }
        }

        private void txtHrzone_Click(object sender, EventArgs e)
        {
            txtHrzone.Text = "";
        }

        private void txtHrzone_Leave(object sender, EventArgs e)
        {
            if (this.txtHrzone.TextLength == 0)
            {
                txtHrzone.Text = "Max heart rate";
            }
        }

        private void allCalculationsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            HR = true;
            SP = false;
            CA = false;
            ALT = false;
            PO = false;

            cbHR.Checked = true;
            cbspeed.Checked = false;
            cbpower.Checked = false;
            cbcadence.Checked = false;
            cbaltitude.Checked = false;

            CreateGraph(zedGraphControl1);
            Calculations(xmin, xmax);


        }

        private void speedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HR = false;
            SP = true;
            CA = false;
            ALT = false;
            PO = false;

            cbHR.Checked = false;
            cbspeed.Checked = true;
            cbpower.Checked = false;
            cbcadence.Checked = false;
            cbaltitude.Checked = false;

            CreateGraph(zedGraphControl1);
            Calculations(xmin, xmax);
        }

        private void cadenceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HR = false;
            SP = false;
            CA = true;
            ALT = false;
            PO = false;

            cbHR.Checked = false;
            cbspeed.Checked = false;
            cbpower.Checked = false;
            cbcadence.Checked = true;
            cbaltitude.Checked = false;

            CreateGraph(zedGraphControl1);
            Calculations(xmin, xmax);
        }

        private void powerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HR = false;
            SP = false;
            CA = false;
            ALT = false;
            PO = true;


            cbHR.Checked = false;
            cbspeed.Checked = false;
            cbpower.Checked = true;
            cbcadence.Checked = false;
            cbaltitude.Checked = false;


            CreateGraph(zedGraphControl1);
            Calculations(xmin, xmax);
        }

        private void altitudeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HR = false;
            SP = false;
            CA = false;
            ALT = true;
            PO = false;

            cbHR.Checked = false;
            cbspeed.Checked = false;
            cbpower.Checked = false;
            cbcadence.Checked = false;
            cbaltitude.Checked = true;

            CreateGraph(zedGraphControl1);
            Calculations(xmin, xmax);
        }

        private void btnpowerzone_Click(object sender, EventArgs e)
        {
             bool check = true;
            if (txtpwrzone.Text == "Functional Threshold Power")
            {
                MessageBox.Show("Please enter your functional threshold power");
                check = false;
            }
            if (txtpwrzone.TextLength > 2 && check == true)
            {
            int maxhr, min, max;
            maxhr = Convert.ToInt32(txtpwrzone.Text);
            min = Convert.ToInt32(xmin);
            max = Convert.ToInt32(xmax);
            PieChart heart = new PieChart(Fname, min, max, maxhr, "power");
            heart.Show();
            }
            else if (check == true)
            {
                MessageBox.Show("Please enter your functional threshold power");
            }
        }

        private void txtpwrzone_Click(object sender, EventArgs e)
        {
            txtpwrzone.Text = "";
        }

        private void txtpwrzone_Leave(object sender, EventArgs e)
        {
            if (this.txtpwrzone.TextLength == 0)
            {
                txtpwrzone.Text = "Functional Threshold Power";
            }
        }

        private void txtMin_TextChanged(object sender, EventArgs e)
        {

        }

        private void cBNP_CheckedChanged(object sender, EventArgs e)
        {
            if (cBNP.Checked == true)
            {
                NP = true;
            }
            else
                NP = false;
            txtNP.Text = "";
            txtIF.Text = "";
            txtTSS.Text = "";
            CreateGraph(zedGraphControl1);
            Calculations(xmin, xmax);
        }

     
    }
}
