﻿namespace CycleAssignment
{
    partial class GraphForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HRTool = new System.Windows.Forms.ToolStripMenuItem();
            this.SPTool = new System.Windows.Forms.ToolStripMenuItem();
            this.CATool = new System.Windows.Forms.ToolStripMenuItem();
            this.ALTTool = new System.Windows.Forms.ToolStripMenuItem();
            this.POTool = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Location = new System.Drawing.Point(12, 173);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0D;
            this.zedGraphControl1.ScrollMaxX = 0D;
            this.zedGraphControl1.ScrollMaxY = 0D;
            this.zedGraphControl1.ScrollMaxY2 = 0D;
            this.zedGraphControl1.ScrollMinX = 0D;
            this.zedGraphControl1.ScrollMinY = 0D;
            this.zedGraphControl1.ScrollMinY2 = 0D;
            this.zedGraphControl1.Size = new System.Drawing.Size(794, 363);
            this.zedGraphControl1.TabIndex = 0;
            this.zedGraphControl1.Resize += new System.EventHandler(this.zedGraphControl1_Resize);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.dataToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(816, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewDataToolStripMenuItem,
            this.openToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // viewDataToolStripMenuItem
            // 
            this.viewDataToolStripMenuItem.Name = "viewDataToolStripMenuItem";
            this.viewDataToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.viewDataToolStripMenuItem.Text = "View data";
            this.viewDataToolStripMenuItem.Click += new System.EventHandler(this.viewDataToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // dataToolStripMenuItem
            // 
            this.dataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.HRTool,
            this.SPTool,
            this.CATool,
            this.ALTTool,
            this.POTool});
            this.dataToolStripMenuItem.Name = "dataToolStripMenuItem";
            this.dataToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.dataToolStripMenuItem.Text = "Data";
            // 
            // HRTool
            // 
            this.HRTool.Checked = true;
            this.HRTool.CheckOnClick = true;
            this.HRTool.CheckState = System.Windows.Forms.CheckState.Checked;
            this.HRTool.Name = "HRTool";
            this.HRTool.Size = new System.Drawing.Size(152, 22);
            this.HRTool.Text = "Heart rate";
            this.HRTool.Click += new System.EventHandler(this.HRTool_Click);
            // 
            // SPTool
            // 
            this.SPTool.Checked = true;
            this.SPTool.CheckOnClick = true;
            this.SPTool.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SPTool.Name = "SPTool";
            this.SPTool.Size = new System.Drawing.Size(152, 22);
            this.SPTool.Text = "Speed";
            this.SPTool.Click += new System.EventHandler(this.SPTool_Click);
            // 
            // CATool
            // 
            this.CATool.Checked = true;
            this.CATool.CheckOnClick = true;
            this.CATool.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CATool.Name = "CATool";
            this.CATool.Size = new System.Drawing.Size(152, 22);
            this.CATool.Text = "Cadence";
            this.CATool.Click += new System.EventHandler(this.CATool_Click);
            // 
            // ALTTool
            // 
            this.ALTTool.Checked = true;
            this.ALTTool.CheckOnClick = true;
            this.ALTTool.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ALTTool.Name = "ALTTool";
            this.ALTTool.Size = new System.Drawing.Size(152, 22);
            this.ALTTool.Text = "Altitude";
            this.ALTTool.Click += new System.EventHandler(this.ALTTool_Click);
            // 
            // POTool
            // 
            this.POTool.Checked = true;
            this.POTool.CheckOnClick = true;
            this.POTool.CheckState = System.Windows.Forms.CheckState.Checked;
            this.POTool.Name = "POTool";
            this.POTool.Size = new System.Drawing.Size(152, 22);
            this.POTool.Text = "Power";
            this.POTool.Click += new System.EventHandler(this.POTool_Click);
            // 
            // GraphForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 548);
            this.Controls.Add(this.zedGraphControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "GraphForm";
            this.Text = "GraphForm";
            this.Load += new System.EventHandler(this.GraphForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HRTool;
        private System.Windows.Forms.ToolStripMenuItem SPTool;
        private System.Windows.Forms.ToolStripMenuItem CATool;
        private System.Windows.Forms.ToolStripMenuItem ALTTool;
        private System.Windows.Forms.ToolStripMenuItem POTool;
    }
}