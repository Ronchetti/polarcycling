﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace CycleAssignment
{
    public partial class GraphForm : Form
    {
        string Fname = @"I:\ASEb\test.txt";
        bool HR = true;
        bool SP = true;
        bool CA = true;
        bool ALT = true;
        bool PO = true;

        public GraphForm()
        {
            InitializeComponent();
            openfile();
        }

        private void viewDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 dataform = new Form1(Fname);
            dataform.Show();
        }

        private void GraphForm_Load(object sender, EventArgs e)
        {
            // Setup the graph
            CreateGraph(zedGraphControl1);
            // Size the control to fill the form with a margin
            SetSize();
        }

        public void openfile()
        {
            //DialogResult File = openFileDialog1.ShowDialog();
            OpenFileDialog theDialog = new OpenFileDialog();
            theDialog.Title = "Open Text File";
            //theDialog.Filter = "TXT files|*.txt";
            theDialog.InitialDirectory = @"I:\ASEb\";
            if (theDialog.ShowDialog(this) == DialogResult.OK)
            {
                //Assigns the file name to a global string then reads the file
                Fname = theDialog.FileName.ToString();
            }
        }

        private void zedGraphControl1_Resize(object sender, EventArgs e)
        {
            SetSize();
        }

        // SetSize() is separate from Resize() so we can 
        // call it independently from the Form1_Load() method
        // This leaves a 10 px margin around the outside of the control
        // Customize this to fit your needs
        private void SetSize()
        {
            zedGraphControl1.Location = new Point(10, 10);
            // Leave a small margin around the outside of the control
            zedGraphControl1.Size = new Size(ClientRectangle.Width - 20,
                                    ClientRectangle.Height - 20);
        }

        // Build the Chart
        private void CreateGraph(ZedGraphControl zgc)
        {
            

            Form1 form1 = new Form1(Fname);
            // get a reference to the GraphPane
            GraphPane myPane = zgc.GraphPane;
            myPane.CurveList.Clear();
            myPane.GraphObjList.Clear();

            // Set the Titles
            myPane.Title.Text = "Polar cycling";
            myPane.XAxis.Title.Text = "Time";
            myPane.YAxis.Title.Text = "My Y Axis";

            //HR speed cadence alt power power balance time

            // Make up some data arrays based on the Sine function
            double x, y1, y2, y3, y4, y5, inc;
            inc = Convert.ToDouble(form1.txtinterval.Text);
            x = 0;
            PointPairList listHR = new PointPairList();
            PointPairList listSp = new PointPairList();
            PointPairList listCad = new PointPairList();
            PointPairList listAlt = new PointPairList();
            PointPairList listPow = new PointPairList();

            //Add power and distance
            for (int i = 0; i < form1.dgvData.Rows.Count; ++i)
            {
              
                //Heart rate
                y1 = Convert.ToDouble(form1.dgvData.Rows[i].Cells[0].Value);
                //Speed
                y2 = Convert.ToDouble(form1.dgvData.Rows[i].Cells[1].Value);
                //Cadence
                y3 = Convert.ToDouble(form1.dgvData.Rows[i].Cells[2].Value);
                //Altitude
                y4 = Convert.ToDouble(form1.dgvData.Rows[i].Cells[3].Value);
                //Power
                y5 = Convert.ToDouble(form1.dgvData.Rows[i].Cells[4].Value);

                listHR.Add(x, y1);
                listSp.Add(x, y2);
                listCad.Add(x, y3);
                listAlt.Add(x, y4);
                listPow.Add(x, y5);
                x = x + inc;
            }

            //Generate curve
            if (HR == true)
            {
                LineItem heart = myPane.AddCurve("Heart rate",
                      listHR, Color.Red, SymbolType.None);
            }

            if (SP == true)
            {
                LineItem speed = myPane.AddCurve("Speed",
                      listSp, Color.Blue, SymbolType.None);
            }

            if (CA == true)
            {
                LineItem cadence = myPane.AddCurve("Cadence",
                   listCad, Color.CornflowerBlue, SymbolType.None);
            }

            if (ALT == true)
            {
                LineItem altitude = myPane.AddCurve("Altitude",
                   listAlt, Color.Crimson, SymbolType.None);
            }

            if (PO == true)
            {
                LineItem power = myPane.AddCurve("Power",
                   listPow, Color.DarkMagenta, SymbolType.None);
            }

            // Tell ZedGraph to refigure the
            // axes since the data have changed
            myPane.XAxis.Scale.Max = x + 20;
            zgc.AxisChange();
            zgc.Refresh();
            
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openfile();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void HRTool_Click(object sender, EventArgs e)
        {
            if (HRTool.Checked == true)
            {
                HR = true;
            }
            else
                HR = false;
            CreateGraph(zedGraphControl1);
        }

        private void SPTool_Click(object sender, EventArgs e)
        {
            if (SPTool.Checked == true)
            {
                SP = true;
            }
            else
                SP = false;
            CreateGraph(zedGraphControl1);
        }

        private void CATool_Click(object sender, EventArgs e)
        {
            if (CATool.Checked == true)
            {
                CA = true;
            }
            else
                CA = false;
            CreateGraph(zedGraphControl1);
        }

        private void ALTTool_Click(object sender, EventArgs e)
        {
            if (ALTTool.Checked == true)
            {
                ALT = true;
            }
            else
                ALT = false;
            CreateGraph(zedGraphControl1);
        }

        private void POTool_Click(object sender, EventArgs e)
        {
            if (POTool.Checked == true)
            {
                PO = true;
            }
            else
                PO = false;
            CreateGraph(zedGraphControl1);
        }

    }
}
