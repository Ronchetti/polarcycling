﻿namespace CycleAssignment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.totalDistanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.averageSpeedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maximumSpeedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.averageHeartRateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maximumHeartRateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minimumHeartRateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.averagePowerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maximumPowerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.averageAltitudeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maximumAltitudeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblsummary = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.txtTime = new System.Windows.Forms.TextBox();
            this.txtsmode = new System.Windows.Forms.TextBox();
            this.txtLength = new System.Windows.Forms.TextBox();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblsmode = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblInterval = new System.Windows.Forms.Label();
            this.txtinterval = new System.Windows.Forms.TextBox();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.rbUS = new System.Windows.Forms.RadioButton();
            this.rbEuro = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.lblMonitor = new System.Windows.Forms.Label();
            this.txtmonitor = new System.Windows.Forms.TextBox();
            this.summaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.dataToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1019, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // dataToolStripMenuItem
            // 
            this.dataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.totalDistanceToolStripMenuItem,
            this.averageSpeedToolStripMenuItem,
            this.maximumSpeedToolStripMenuItem,
            this.averageHeartRateToolStripMenuItem,
            this.maximumHeartRateToolStripMenuItem,
            this.minimumHeartRateToolStripMenuItem,
            this.averagePowerToolStripMenuItem,
            this.maximumPowerToolStripMenuItem,
            this.averageAltitudeToolStripMenuItem,
            this.maximumAltitudeToolStripMenuItem,
            this.summaryToolStripMenuItem});
            this.dataToolStripMenuItem.Name = "dataToolStripMenuItem";
            this.dataToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.dataToolStripMenuItem.Text = "Data";
            // 
            // totalDistanceToolStripMenuItem
            // 
            this.totalDistanceToolStripMenuItem.Name = "totalDistanceToolStripMenuItem";
            this.totalDistanceToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.totalDistanceToolStripMenuItem.Text = "Total distance";
            this.totalDistanceToolStripMenuItem.Click += new System.EventHandler(this.totalDistanceToolStripMenuItem_Click);
            // 
            // averageSpeedToolStripMenuItem
            // 
            this.averageSpeedToolStripMenuItem.Name = "averageSpeedToolStripMenuItem";
            this.averageSpeedToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.averageSpeedToolStripMenuItem.Text = "Average speed";
            this.averageSpeedToolStripMenuItem.Click += new System.EventHandler(this.averageSpeedToolStripMenuItem_Click);
            // 
            // maximumSpeedToolStripMenuItem
            // 
            this.maximumSpeedToolStripMenuItem.Name = "maximumSpeedToolStripMenuItem";
            this.maximumSpeedToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.maximumSpeedToolStripMenuItem.Text = "Maximum speed";
            this.maximumSpeedToolStripMenuItem.Click += new System.EventHandler(this.maximumSpeedToolStripMenuItem_Click);
            // 
            // averageHeartRateToolStripMenuItem
            // 
            this.averageHeartRateToolStripMenuItem.Name = "averageHeartRateToolStripMenuItem";
            this.averageHeartRateToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.averageHeartRateToolStripMenuItem.Text = "Average Heart Rate";
            this.averageHeartRateToolStripMenuItem.Click += new System.EventHandler(this.averageHeartRateToolStripMenuItem_Click);
            // 
            // maximumHeartRateToolStripMenuItem
            // 
            this.maximumHeartRateToolStripMenuItem.Name = "maximumHeartRateToolStripMenuItem";
            this.maximumHeartRateToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.maximumHeartRateToolStripMenuItem.Text = "Maximum Heart Rate";
            this.maximumHeartRateToolStripMenuItem.Click += new System.EventHandler(this.maximumHeartRateToolStripMenuItem_Click);
            // 
            // minimumHeartRateToolStripMenuItem
            // 
            this.minimumHeartRateToolStripMenuItem.Name = "minimumHeartRateToolStripMenuItem";
            this.minimumHeartRateToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.minimumHeartRateToolStripMenuItem.Text = "Minimum Heart Rate";
            this.minimumHeartRateToolStripMenuItem.Click += new System.EventHandler(this.minimumHeartRateToolStripMenuItem_Click);
            // 
            // averagePowerToolStripMenuItem
            // 
            this.averagePowerToolStripMenuItem.Name = "averagePowerToolStripMenuItem";
            this.averagePowerToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.averagePowerToolStripMenuItem.Text = "Average Power";
            this.averagePowerToolStripMenuItem.Click += new System.EventHandler(this.averagePowerToolStripMenuItem_Click);
            // 
            // maximumPowerToolStripMenuItem
            // 
            this.maximumPowerToolStripMenuItem.Name = "maximumPowerToolStripMenuItem";
            this.maximumPowerToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.maximumPowerToolStripMenuItem.Text = "Maximum power";
            this.maximumPowerToolStripMenuItem.Click += new System.EventHandler(this.maximumPowerToolStripMenuItem_Click);
            // 
            // averageAltitudeToolStripMenuItem
            // 
            this.averageAltitudeToolStripMenuItem.Name = "averageAltitudeToolStripMenuItem";
            this.averageAltitudeToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.averageAltitudeToolStripMenuItem.Text = "Average altitude";
            this.averageAltitudeToolStripMenuItem.Click += new System.EventHandler(this.averageAltitudeToolStripMenuItem_Click);
            // 
            // maximumAltitudeToolStripMenuItem
            // 
            this.maximumAltitudeToolStripMenuItem.Name = "maximumAltitudeToolStripMenuItem";
            this.maximumAltitudeToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.maximumAltitudeToolStripMenuItem.Text = "Maximum altitude";
            this.maximumAltitudeToolStripMenuItem.Click += new System.EventHandler(this.maximumAltitudeToolStripMenuItem_Click);
            // 
            // lblsummary
            // 
            this.lblsummary.AutoSize = true;
            this.lblsummary.Location = new System.Drawing.Point(13, 39);
            this.lblsummary.Name = "lblsummary";
            this.lblsummary.Size = new System.Drawing.Size(30, 13);
            this.lblsummary.TabIndex = 4;
            this.lblsummary.Text = "Date";
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(16, 55);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(81, 20);
            this.txtDate.TabIndex = 5;
            // 
            // txtTime
            // 
            this.txtTime.Location = new System.Drawing.Point(16, 98);
            this.txtTime.Name = "txtTime";
            this.txtTime.Size = new System.Drawing.Size(81, 20);
            this.txtTime.TabIndex = 5;
            // 
            // txtsmode
            // 
            this.txtsmode.Location = new System.Drawing.Point(16, 141);
            this.txtsmode.Name = "txtsmode";
            this.txtsmode.Size = new System.Drawing.Size(81, 20);
            this.txtsmode.TabIndex = 5;
            // 
            // txtLength
            // 
            this.txtLength.Location = new System.Drawing.Point(16, 185);
            this.txtLength.Name = "txtLength";
            this.txtLength.Size = new System.Drawing.Size(81, 20);
            this.txtLength.TabIndex = 5;
            // 
            // txtVersion
            // 
            this.txtVersion.Location = new System.Drawing.Point(16, 225);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(81, 20);
            this.txtVersion.TabIndex = 5;
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(13, 82);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(30, 13);
            this.lblTime.TabIndex = 6;
            this.lblTime.Text = "Time";
            // 
            // lblsmode
            // 
            this.lblsmode.AutoSize = true;
            this.lblsmode.Location = new System.Drawing.Point(13, 125);
            this.lblsmode.Name = "lblsmode";
            this.lblsmode.Size = new System.Drawing.Size(40, 13);
            this.lblsmode.TabIndex = 7;
            this.lblsmode.Text = "Smode";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 166);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Length";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(13, 209);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(42, 13);
            this.lblVersion.TabIndex = 9;
            this.lblVersion.Text = "Version";
            // 
            // lblInterval
            // 
            this.lblInterval.AutoSize = true;
            this.lblInterval.Location = new System.Drawing.Point(16, 252);
            this.lblInterval.Name = "lblInterval";
            this.lblInterval.Size = new System.Drawing.Size(42, 13);
            this.lblInterval.TabIndex = 10;
            this.lblInterval.Text = "Interval";
            // 
            // txtinterval
            // 
            this.txtinterval.Location = new System.Drawing.Point(16, 269);
            this.txtinterval.Name = "txtinterval";
            this.txtinterval.Size = new System.Drawing.Size(81, 20);
            this.txtinterval.TabIndex = 11;
            // 
            // dgvData
            // 
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Location = new System.Drawing.Point(244, 39);
            this.dgvData.Name = "dgvData";
            this.dgvData.Size = new System.Drawing.Size(763, 302);
            this.dgvData.TabIndex = 12;
            this.dgvData.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellContentClick);
            // 
            // rbUS
            // 
            this.rbUS.AutoSize = true;
            this.rbUS.Location = new System.Drawing.Point(185, 39);
            this.rbUS.Name = "rbUS";
            this.rbUS.Size = new System.Drawing.Size(40, 17);
            this.rbUS.TabIndex = 13;
            this.rbUS.TabStop = true;
            this.rbUS.Text = "US";
            this.rbUS.UseVisualStyleBackColor = true;
            this.rbUS.CheckedChanged += new System.EventHandler(this.rbUS_CheckedChanged);
            // 
            // rbEuro
            // 
            this.rbEuro.AutoSize = true;
            this.rbEuro.Location = new System.Drawing.Point(185, 62);
            this.rbEuro.Name = "rbEuro";
            this.rbEuro.Size = new System.Drawing.Size(47, 17);
            this.rbEuro.TabIndex = 14;
            this.rbEuro.TabStop = true;
            this.rbEuro.Text = "Euro";
            this.rbEuro.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(128, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Units:";
            // 
            // lblMonitor
            // 
            this.lblMonitor.AutoSize = true;
            this.lblMonitor.Location = new System.Drawing.Point(16, 292);
            this.lblMonitor.Name = "lblMonitor";
            this.lblMonitor.Size = new System.Drawing.Size(42, 13);
            this.lblMonitor.TabIndex = 16;
            this.lblMonitor.Text = "Monitor";
            // 
            // txtmonitor
            // 
            this.txtmonitor.Location = new System.Drawing.Point(19, 309);
            this.txtmonitor.Name = "txtmonitor";
            this.txtmonitor.Size = new System.Drawing.Size(78, 20);
            this.txtmonitor.TabIndex = 17;
            // 
            // summaryToolStripMenuItem
            // 
            this.summaryToolStripMenuItem.Name = "summaryToolStripMenuItem";
            this.summaryToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.summaryToolStripMenuItem.Text = "Summary";
            this.summaryToolStripMenuItem.Click += new System.EventHandler(this.summaryToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 355);
            this.Controls.Add(this.txtmonitor);
            this.Controls.Add(this.lblMonitor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rbEuro);
            this.Controls.Add(this.rbUS);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.txtinterval);
            this.Controls.Add(this.lblInterval);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblsmode);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.txtVersion);
            this.Controls.Add(this.txtLength);
            this.Controls.Add(this.txtsmode);
            this.Controls.Add(this.txtTime);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.lblsummary);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.Label lblsummary;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.TextBox txtTime;
        private System.Windows.Forms.TextBox txtsmode;
        private System.Windows.Forms.TextBox txtLength;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblsmode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblInterval;
        private System.Windows.Forms.ToolStripMenuItem dataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem totalDistanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem averageSpeedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maximumSpeedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem averageHeartRateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maximumHeartRateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minimumHeartRateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem averagePowerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maximumPowerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem averageAltitudeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maximumAltitudeToolStripMenuItem;
        private System.Windows.Forms.RadioButton rbUS;
        private System.Windows.Forms.RadioButton rbEuro;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblMonitor;
        private System.Windows.Forms.TextBox txtmonitor;
        public System.Windows.Forms.DataGridView dgvData;
        public System.Windows.Forms.TextBox txtinterval;
        private System.Windows.Forms.ToolStripMenuItem summaryToolStripMenuItem;
    }
}

