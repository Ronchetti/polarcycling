﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CycleAssignment
{
    public partial class Form1 : Form
    {
        //Variables from the header
        string Fname = @"I:\ASEb\test.txt";
        string version = "";
        string smode = "";
        string stime = "";
        string length = "";
        string interval = "";
        int count, total, add = 0;

        //Fname.LastIndexOf(":");    Position of the last :
        //Fname.IndexOf(":");        Position of the first :
        //Fname.Substring(index);    Gets the string after a specified positon
        //Fname.Contains("ASEb");
        //sections = fname.split(":");
        //day = Convert.ToInt32(fname.substring(index+1,2));   Takes the value +1 after the colon and takes two numbers for the day and converts to int
        //month = Convert.ToInt32(fname.substring(index+4,2));   
        //year = Convert.ToInt32(fname.substring(index+7,2));   

        //System.IO.StreamReader myfile = new System.IO.StreamReader(fname);
        //while ((oneline = myfile.readline()) != null)
        //String [] words =
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnread_Click(object sender, EventArgs e)
        {
            
            if (File.Exists(@"I:\ASEb\test.txt")){
            } try  {
                //Catch all runetime errors
                //string filecontent = System.IO.File.ReadAllText(@"I:\ASEb\txt.txt");
                //Reading text using an array
                string[] filelines = System.IO.File.ReadAllLines(@"I:\ASEb\test.txt");
               //foreach (string line in filelines);
                lbxdata.Items.AddRange(filelines);
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could find file");
            }

        }

        private void readfile()
        {
            string dt;
            int point;
            if (File.Exists(Fname))
            {
            } try
            {
                using (StreamReader sr = new StreamReader(Fname))
                {
                    lbxdata.Items.Clear();
                    string line;
                    bool hrdata = false;
                    // Read and display lines from the file until the end of  
                    // the file is reached. 


                    while ((line = sr.ReadLine()) != null)
                    {
                        point = line.IndexOf("=");
                        //Checks for the data
                        if (line.Contains("HRData"))
                        {
                            hrdata = true;
                        }

                        //Only adds to the listbox if it is data
                        if (hrdata == true)
                        {
                            lbxdata.Items.Add(line);
                            count = count + 1;
                        }

                        //Smode grabber
                        if (line.Contains("SMode"))
                        {
                            smode = line.Substring(point + 1);
                            txtsmode.Text = smode;
                        }

                        //Interval grabber
                        if (line.Contains("Interval"))
                        {
                            interval = line.Substring(point + 1);

                        }

                        //Version grabber
                        if (line.Contains("Version"))
                        {
                            version = line.Substring(point + 1);
                            txtVersion.Text = version;

                        }

                        //Start time grabber
                        if (line.Contains("StartTime"))
                        {
                            stime = line.Substring(point + 1);
                            txtTime.Text = stime;

                        }

                        //Length grabber
                        if (line.Contains("Length"))
                        {
                            length = line.Substring(point + 1);
                            txtLength.Text = length;

                        }

                        //Checks for the date
                        if (line.Contains("Date="))
                        {
                            //Grabs the integers after the =                           
                            dt = line.Substring(point + 1);
                            //Passes the date over to be processed
                            dategrabber(dt);
                        }
                    }
                    //Removes unwanted text at top of listbox
                    lbxdata.Items.RemoveAt(lbxdata.TopIndex);
                    //Accounts for deleting the false value
                    count = count - 1;
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find file");
            }
        }

        private void dategrabber(string date)
        {
            int year, month, day;

            year = Convert.ToInt32(date.Substring(0, 4));
            month = Convert.ToInt32(date.Substring(4, 2));
            day = Convert.ToInt32(date.Substring(6, 2));
            DateTime datet = new DateTime(year, month, day);
            //MessageBox.Show("Date: " + datet + smode + version);
           // txtDate.Text = datet;

        }

        private void toolstripopen_Click(object sender, EventArgs e)
        {

        }


        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //DialogResult File = openFileDialog1.ShowDialog();
            OpenFileDialog theDialog = new OpenFileDialog();
            theDialog.Title = "Open Text File";
            theDialog.Filter = "TXT files|*.txt";
            theDialog.InitialDirectory = @"I:\ASEb\";
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                //Assigns the file name to a global string then reads the file
                Fname = theDialog.FileName.ToString();
                readfile();
            }
        }

        private void btnDate_Click(object sender, EventArgs e)
        {
         
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
    

}
