﻿namespace CycleAssignment
{
    partial class PieChart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PieChart1 = new ZedGraph.ZedGraphControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.lblzone1 = new System.Windows.Forms.Label();
            this.lblzone2text = new System.Windows.Forms.Label();
            this.lblzone2 = new System.Windows.Forms.Label();
            this.lblzone3text = new System.Windows.Forms.Label();
            this.lblzone3 = new System.Windows.Forms.Label();
            this.lblzone4text = new System.Windows.Forms.Label();
            this.lblzone4 = new System.Windows.Forms.Label();
            this.lblzone5text = new System.Windows.Forms.Label();
            this.lblzone5 = new System.Windows.Forms.Label();
            this.lblzone6text = new System.Windows.Forms.Label();
            this.lblZone6 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // PieChart1
            // 
            this.PieChart1.Location = new System.Drawing.Point(244, 12);
            this.PieChart1.Name = "PieChart1";
            this.PieChart1.Padding = new System.Windows.Forms.Padding(0, 20, 0, 0);
            this.PieChart1.ScrollGrace = 0D;
            this.PieChart1.ScrollMaxX = 0D;
            this.PieChart1.ScrollMaxY = 0D;
            this.PieChart1.ScrollMaxY2 = 0D;
            this.PieChart1.ScrollMinX = 0D;
            this.PieChart1.ScrollMinY = 0D;
            this.PieChart1.ScrollMinY2 = 0D;
            this.PieChart1.Size = new System.Drawing.Size(746, 618);
            this.PieChart1.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.lblzone1);
            this.panel1.Controls.Add(this.lblzone2text);
            this.panel1.Controls.Add(this.lblzone2);
            this.panel1.Controls.Add(this.lblzone3text);
            this.panel1.Controls.Add(this.lblzone3);
            this.panel1.Controls.Add(this.lblzone4text);
            this.panel1.Controls.Add(this.lblzone4);
            this.panel1.Controls.Add(this.lblzone5text);
            this.panel1.Controls.Add(this.lblzone5);
            this.panel1.Controls.Add(this.lblzone6text);
            this.panel1.Controls.Add(this.lblZone6);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(226, 618);
            this.panel1.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(5, 552);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(218, 66);
            this.label10.TabIndex = 2;
            this.label10.Text = "For long, easy rides, to improve the combustion and storage of fats.";
            // 
            // lblzone1
            // 
            this.lblzone1.AutoSize = true;
            this.lblzone1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblzone1.ForeColor = System.Drawing.Color.Teal;
            this.lblzone1.Location = new System.Drawing.Point(-4, 528);
            this.lblzone1.Name = "lblzone1";
            this.lblzone1.Size = new System.Drawing.Size(136, 24);
            this.lblzone1.TabIndex = 0;
            this.lblzone1.Text = "Zone 1 60-65%";
            // 
            // lblzone2text
            // 
            this.lblzone2text.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblzone2text.Location = new System.Drawing.Point(5, 450);
            this.lblzone2text.Name = "lblzone2text";
            this.lblzone2text.Size = new System.Drawing.Size(218, 78);
            this.lblzone2text.TabIndex = 2;
            this.lblzone2text.Text = "The basic base training zone. Longish rides of medium stress.";
            // 
            // lblzone2
            // 
            this.lblzone2.AutoSize = true;
            this.lblzone2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblzone2.ForeColor = System.Drawing.Color.Turquoise;
            this.lblzone2.Location = new System.Drawing.Point(-4, 426);
            this.lblzone2.Name = "lblzone2";
            this.lblzone2.Size = new System.Drawing.Size(136, 24);
            this.lblzone2.TabIndex = 0;
            this.lblzone2.Text = "Zone 2 65-75%";
            // 
            // lblzone3text
            // 
            this.lblzone3text.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblzone3text.Location = new System.Drawing.Point(5, 346);
            this.lblzone3text.Name = "lblzone3text";
            this.lblzone3text.Size = new System.Drawing.Size(218, 80);
            this.lblzone3text.TabIndex = 2;
            this.lblzone3text.Text = " For development of aerobic capacity and endurance with moderate volume at very c" +
    "ontrolled intensity.";
            // 
            // lblzone3
            // 
            this.lblzone3.AutoSize = true;
            this.lblzone3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblzone3.ForeColor = System.Drawing.Color.LimeGreen;
            this.lblzone3.Location = new System.Drawing.Point(-4, 322);
            this.lblzone3.Name = "lblzone3";
            this.lblzone3.Size = new System.Drawing.Size(136, 24);
            this.lblzone3.TabIndex = 0;
            this.lblzone3.Text = "Zone 3 75-82%";
            // 
            // lblzone4text
            // 
            this.lblzone4text.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblzone4text.Location = new System.Drawing.Point(5, 250);
            this.lblzone4text.Name = "lblzone4text";
            this.lblzone4text.Size = new System.Drawing.Size(218, 72);
            this.lblzone4text.TabIndex = 2;
            this.lblzone4text.Text = "For simulating pace when tapering for a race.";
            // 
            // lblzone4
            // 
            this.lblzone4.AutoSize = true;
            this.lblzone4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblzone4.ForeColor = System.Drawing.Color.Goldenrod;
            this.lblzone4.Location = new System.Drawing.Point(-5, 226);
            this.lblzone4.Name = "lblzone4";
            this.lblzone4.Size = new System.Drawing.Size(136, 24);
            this.lblzone4.TabIndex = 0;
            this.lblzone4.Text = "Zone 4 82-89%";
            // 
            // lblzone5text
            // 
            this.lblzone5text.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblzone5text.Location = new System.Drawing.Point(5, 126);
            this.lblzone5text.Name = "lblzone5text";
            this.lblzone5text.Size = new System.Drawing.Size(218, 84);
            this.lblzone5text.TabIndex = 2;
            this.lblzone5text.Text = "For raising anaerobic threshold. Good sessions for 10- and 25-mile time-trials.";
            // 
            // lblzone5
            // 
            this.lblzone5.AutoSize = true;
            this.lblzone5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblzone5.ForeColor = System.Drawing.Color.DarkOrange;
            this.lblzone5.Location = new System.Drawing.Point(-4, 102);
            this.lblzone5.Name = "lblzone5";
            this.lblzone5.Size = new System.Drawing.Size(136, 24);
            this.lblzone5.TabIndex = 0;
            this.lblzone5.Text = "Zone 5 89-94%";
            // 
            // lblzone6text
            // 
            this.lblzone6text.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblzone6text.Location = new System.Drawing.Point(5, 24);
            this.lblzone6text.Name = "lblzone6text";
            this.lblzone6text.Size = new System.Drawing.Size(218, 78);
            this.lblzone6text.TabIndex = 2;
            this.lblzone6text.Text = " For high-intensity interval training to increase maximum power and speed";
            // 
            // lblZone6
            // 
            this.lblZone6.AutoSize = true;
            this.lblZone6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblZone6.ForeColor = System.Drawing.Color.Red;
            this.lblZone6.Location = new System.Drawing.Point(-4, 0);
            this.lblZone6.Name = "lblZone6";
            this.lblZone6.Size = new System.Drawing.Size(146, 24);
            this.lblZone6.TabIndex = 0;
            this.lblZone6.Text = "Zone 6 94-100%";
            // 
            // PieChart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 641);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.PieChart1);
            this.Name = "PieChart";
            this.Text = "PieChart";
            this.Load += new System.EventHandler(this.PieChart_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ZedGraph.ZedGraphControl PieChart1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblzone1;
        private System.Windows.Forms.Label lblzone2text;
        private System.Windows.Forms.Label lblzone2;
        private System.Windows.Forms.Label lblzone3text;
        private System.Windows.Forms.Label lblzone3;
        private System.Windows.Forms.Label lblzone4text;
        private System.Windows.Forms.Label lblzone4;
        private System.Windows.Forms.Label lblzone5text;
        private System.Windows.Forms.Label lblzone5;
        private System.Windows.Forms.Label lblzone6text;
        private System.Windows.Forms.Label lblZone6;
    }
}